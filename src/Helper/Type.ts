import { ObjectId } from "mongoose"

export interface IJWTPayload {
    _id: string
}

export interface IUser {
    name: string,
    username: string,
    email: string,
    password: string,
    photo: string,
    token?: string,
    createdAt?: Date,
    updatedAt?: Date
}

export interface IPost {
    userId: ObjectId,
    caption?: string,
    tags?: string,
    likes?: number,
    image: string,
    createdAt?: Date,
    updatedAt?: Date
}

export interface IUserLiked {
    postId: ObjectId,
    userId: ObjectId
}

export interface IImageUpload {
    name: string,
    url: string,
    mimetype: string
}