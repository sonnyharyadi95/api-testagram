import { Response } from "express";

const SUCCESS_MESSAGE = "Yay, success load data.";
const ERROR_MESSAGE = "Oops, failed load data.";
const VALIDATION_MESSAGE = "Oops, validation error.";

export const successResponse = (res: Response, data: any, message?: String) => {
    return res.status(200).json({
        status: true,
        message: message ? message : SUCCESS_MESSAGE,
        data: data,
    });
};

export const successResponseWithPagination = (res: Response, data: any, pagination: any, message?: String) => {
    return res.status(200).json({
        status: true,
        message: message ? message : SUCCESS_MESSAGE,
        data: data,
        pagination: pagination
    });
}

export const errorResponse = (res: Response, message = ERROR_MESSAGE) => {
    return res.status(500).json({
        status: false,
        message: message,
        data: null,
    });
};

export const validationResponse = (
    res: Response,
    data: any,
    message = VALIDATION_MESSAGE
) => {
    return res.status(400).json({
        status: false,
        message: message,
        data: data,
    });
};

export const notFoundResponse = (res: Response, message: String) => {
    return res.status(404).json({
        status: false,
        message: message,
    });
};

export const unauthorizedResponse = (res: Response, message: String) => {
    return res.status(401).json({
        status: false,
        message: message,
    });
};

export const badRequestResponse = (res: Response, message: String) => {
    return res.status(400).json({
        status: false,
        message: message,
    });
}