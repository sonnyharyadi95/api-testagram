import jsonwebtoken from "jsonwebtoken";
import { SECRET_KEY } from "../Helper/Constant";
import { Request, Response } from "express";

export const generateToken = (data: any) => {
    return jsonwebtoken.sign(data, SECRET_KEY, {
        expiresIn: "1h",
    });
};

export const verifyToken = (req: Request, res: Response, next: any) => {
    const bearerHeader = req.header("Authorization");
    if (!bearerHeader) {
        res.status(401);
        return next("Unauthorized Access 1")
    }
    const bearer = bearerHeader.split(" ");
    const token = bearer[1];

    if (!token) {
        res.status(401);
        return next("Unauthorized Access 2")
    }

    jsonwebtoken.verify(token, SECRET_KEY, (err: any, auth: any) => {
        if (err) {
            res.status(401);
            return next(err)
        }
    });
    req.body.token = token;

    next();
};
