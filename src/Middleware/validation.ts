import { Joi } from "express-validation";
import { PHOTO_PATTERN } from "../Helper/Constant";

export const PostValidation = {
    createUpdatePost: {
        body: Joi.object().required().keys({
            image: Joi.string().exist().regex(new RegExp(PHOTO_PATTERN)).messages({
                "string.pattern.base": "Not a Valid Image URL"
            }),
            caption: Joi.string(),
            tags: Joi.string()
        })
    },
    listPost: {
        query: Joi.object().required().keys({
            page: Joi.number().exist(),
            limit: Joi.number().exist(),
            searchBy: Joi.string().valid("caption", "tags").exist(),
            search: Joi.string().allow('').allow(null)
        })
    }
}

export const UserValidation = {
    login: {
        body: Joi.object().required().keys({
            username: Joi.string(),
            password: Joi.string()
        })
    },
    register: {
        body: Joi.object().required().keys({
            name: Joi.string().exist(),
            username: Joi.string().exist(),
            password: Joi.string().exist(),
            email: Joi.string().email().exist(),
            photo: Joi.string().exist().regex(new RegExp(PHOTO_PATTERN)).messages({
                "string.pattern.base": "Not a Valid Image URL"
            }),
        })
    },
    updateUser: {
        body: Joi.object().required().keys({
            name: Joi.string(),
            username: Joi.string(),
            email: Joi.string().email(),
            photo: Joi.string().exist().regex(new RegExp(PHOTO_PATTERN)).messages({
                "string.pattern.base": "Not a Valid Image URL"
            }),
        }),
    },
    changePassword: {
        body: Joi.object().required().keys({
            oldPassword: Joi.string().exist(),
            newPassword: Joi.string().exist(),
            confirmedNewPassword: Joi.string().exist()
        })
    }
}