import { connect } from 'mongoose';
import { MONGODB_URI } from '../Helper/Constant';
const uri: string = MONGODB_URI || '';
export const connectDb = async (): Promise<void> => {
    await connect(uri)
        .then(() => {
            console.log(`MongoDB Running `, MONGODB_URI)
        })
        .catch((err) => {
            console.log(`MongoDB Error : ${err.message}`)
        })
}