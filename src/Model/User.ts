import { Schema, model } from 'mongoose';
import { IUser } from "../Helper/Type";

const schema = new Schema<IUser>({
    name: {
        type: String,
        required: true,
    },
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    photo: {
        type: String,
        required: false
    }
},
    {
        timestamps: true
    });

export const User = model('User', schema);