import { Schema, model } from 'mongoose';
import { IPost } from "../Helper/Type";

const schema = new Schema<IPost>({
    userId: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    caption: {
        type: String,
        required: false
    },
    tags: {
        type: String,
        required: false
    },
    likes: {
        type: Number,
        required: false
    },
    image: {
        type: String,
        required: true
    }
},
    {
        timestamps: true
    });

export const Post = model('Post', schema);