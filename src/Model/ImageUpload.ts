import { Schema, model } from 'mongoose';
import { IImageUpload } from "../Helper/Type";

const schema = new Schema<IImageUpload>({
    name: {
        type: String,
        required: true,
    },
    url: {
        type: String,
        required: true
    },
    mimetype: {
        type: String,
        required: true
    }
});

export const ImageUpload = model('ImageUpload', schema);