import { Schema, model } from 'mongoose';
import { IUserLiked } from "../Helper/Type";

const schema = new Schema<IUserLiked>({
    postId: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    userId: {
        type: Schema.Types.ObjectId,
        required: true
    }
},
    {
        timestamps: true
    });

export const UserLiked = model('UserLiked', schema);