import { User } from "../Model/User";
import { Request, Response } from "express";
import { compare, hash } from "bcryptjs";
import { ACCOUNT_CREATED, ACCOUNT_GET_USER, ACCOUNT_LOGGED_OUT, ACCOUNT_PASS_UPDATED, ACCOUNT_UPDATED, DATA_NOT_FOUND, ERROR_UPDATE, ERROR_USER_EXIST, MAIL_EXIST, NEW_PASS_EXIST, PASSWORD_NOT_MATCH, USER_PASS_INVALID } from "../Helper/Constant";
import { errorResponse, successResponse } from "../Helper/Response";
import { generateToken } from "../Middleware/jwt";
import jwt_decode from "jwt-decode";
import { IJWTPayload } from "../Helper/Type";

export const Register = (req: Request, res: Response) => {
    const { name, username, email, password, photo } = req.body;

    User.findOne({ email: email }).then(async (found) => {
        if (found) {
            return errorResponse(res, MAIL_EXIST)
        }

        const UserStore = new User({
            name: name,
            username: username,
            email: email,
            password: await hash(password, 10),
            photo: photo
        });

        UserStore.save().then((user) => {
            return successResponse(res, {
                name: user.name,
                username: user.username,
                email: user.email,
                photo: user.photo
            }, ACCOUNT_CREATED);
        }).catch((e) => errorResponse(res, e))
    })
}

export const Login = (req: Request, res: Response) => {
    const { username, password } = req.body;
    User.findOne({ username: username }).then((user) => {
        if (!user) {
            return errorResponse(res, USER_PASS_INVALID)
        }
        compare(password, user.password).then((same) => {
            if (!same) {
                return errorResponse(res, USER_PASS_INVALID);
            }

            const token = generateToken({ _id: user.id });
            return successResponse(res, { token: token });
        })
    })
}

export const Logout = (req: Request, res: Response) => {
    return successResponse(res, null, ACCOUNT_LOGGED_OUT);
}

export const UpdatePassword = (req: Request, res: Response) => {
    const token = req.body.token;
    const id = (jwt_decode(token) as IJWTPayload)._id;
    const { oldPassword, newPassword, confirmedNewPassword } = req.body


    User.findById(id).then((user) => {
        if (!user) {
            return successResponse(res, USER_PASS_INVALID)
        }
        compare(oldPassword, user.password).then((same) => {
            if (!same || (newPassword != confirmedNewPassword)) {
                return errorResponse(res, PASSWORD_NOT_MATCH)
            }
            hash(newPassword, 10).then((newHashedPassword) => {
                User.findByIdAndUpdate(id, { password: newHashedPassword }).then((user) => {
                    if (!user) {
                        return errorResponse(res, USER_PASS_INVALID)
                    }
                    return successResponse(res, null, ACCOUNT_PASS_UPDATED);
                }).catch((e) => errorResponse(res, e))
            })
        })
    })
}

export const UpdateUserData = async (req: Request, res: Response) => {
    const token = req.body.token;
    const id = (jwt_decode(token) as IJWTPayload)._id;

    const datas = req.body;
    delete datas.token;
    let mailFound: Boolean | null = false;

    if (datas.hasOwnProperty('email')) {
        mailFound = await User.findOne({ email: datas.email });
    }
    if (mailFound) {
        return errorResponse(res, MAIL_EXIST);
    }

    User.findByIdAndUpdate(id, datas).then((user) => {
        if (!user) {
            return errorResponse(res, ERROR_UPDATE)
        }

        let userData = {
            name: user.name,
            username: user.username,
            email: user.email,
            photo: user.photo,
            createdAt: user.createdAt,
            updatedAt: user.updatedAt

        }

        return successResponse(res, userData, ACCOUNT_UPDATED)
    }).catch((e) => errorResponse(res, e))
}

export const GetUserData = (req: Request, res: Response) => {
    const token = req.body.token;
    const id = (jwt_decode(token) as IJWTPayload)._id;

    User.findById(id).then((user) => {
        if (!user) {
            return errorResponse(res, USER_PASS_INVALID)
        }
        let userData = {
            name: user.name,
            username: user.username,
            email: user.email,
            photo: user.photo,
            createdAt: user.createdAt,
            updatedAt: user.updatedAt
        }
        return successResponse(res, userData, ACCOUNT_GET_USER)
    });
}