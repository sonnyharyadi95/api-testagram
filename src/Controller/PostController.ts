import { Post } from "../Model/Post";
import { User } from "../Model/User";
import { UserLiked } from "../Model/UserLiked";
import { Request, Response } from "express";
import jwt_decode from "jwt-decode";
import { IJWTPayload } from "../Helper/Type";
import { errorResponse, successResponse, successResponseWithPagination } from "../Helper/Response";
import { DATA_NOT_FOUND, ERROR_DELETE, ERROR_INSERT, ERROR_UPDATE, POST_CREATED, POST_DELETED, POST_LIKED, POST_LIKE_TWICE, POST_UNLIKED, POST_UPDATED, USER_NOT_FOUND, USER_PASS_INVALID } from "../Helper/Constant";


export const CreatePost = (req: Request, res: Response) => {
    const { image, caption, tags } = req.body;
    const token = req.body.token;
    const userId = (jwt_decode(token) as IJWTPayload)._id;
    // const userId = "63d1d80352cabc084736dd84"

    User.findById(userId).then((user) => {
        if (!user) {
            return errorResponse(res, USER_PASS_INVALID)
        }

        const PostStore = new Post({
            userId: userId,
            caption: caption,
            tags: tags,
            likes: 0,
            image: image
        })

        PostStore.save().then((post) => {
            return successResponse(res, {
                image: post.image,
                caption: post.caption,
                tags: post.tags,
                likes: post.likes,
                createdAt: post.createdAt,
                updatedAt: post.updatedAt,
                user: {
                    name: user.name,
                    username: user.username,
                    email: user.email,
                    photo: user.photo
                }
            }, POST_CREATED)
        }).catch((e) => errorResponse(res, e))
    })
}

export const UpdatePost = (req: Request, res: Response) => {
    const { image, caption, tags } = req.body;
    const token = req.body.token;
    const userId = (jwt_decode(token) as IJWTPayload)._id;
    const postId = req.params.id;

    User.findById(userId).then((user) => {
        if (!user) {
            return errorResponse(res, USER_PASS_INVALID)
        }

        Post.findById(postId).then((post) => {
            if (!post) {
                return errorResponse(res, DATA_NOT_FOUND)
            }

            let PostData = {
                image: image,
                caption: caption,
                tags: tags
            }

            Post.findByIdAndUpdate(postId, PostData).then((post) => {
                if (!post) {
                    return errorResponse(res, ERROR_UPDATE)
                }

                let PostData = {
                    image: post.image,
                    caption: post.caption,
                    tags: post.tags,
                    likes: post.likes,
                    createdAt: post.createdAt,
                    updatedAt: post.updatedAt,
                    user: {
                        name: user.name,
                        username: user.username,
                        email: user.email,
                        photo: user.photo
                    }
                }

                return successResponse(res, PostData, POST_UPDATED)

            }).catch((e) => errorResponse(res, e))
        }).catch((e) => errorResponse(res, e))
    })
}

export const DeletePost = (req: Request, res: Response) => {
    const postId = req.params.id;
    Post.findByIdAndDelete(postId).then((post) => {
        return !post ? errorResponse(res, ERROR_DELETE) : successResponse(res, null, POST_DELETED)
    }).catch((e) => errorResponse(res, ERROR_DELETE))
}

export const LikePost = (req: Request, res: Response) => {
    const postId = req.params.id;
    const token = req.body.token;
    const userId = (jwt_decode(token) as IJWTPayload)._id;
    const type = req.originalUrl.split("/")[3];


    User.findById(userId).then((user) => {
        if (!user) {
            return errorResponse(res, USER_NOT_FOUND)
        }
        Post.findById(postId).then((post) => {
            if (!post) {
                return errorResponse(res, DATA_NOT_FOUND)
            }

            UserLiked.findOne({ postId: postId, userId: userId }).then((found) => {
                if (type == "like") {
                    if (found) {
                        return errorResponse(res, POST_LIKE_TWICE)
                    }
                    let UserLikeStore = new UserLiked({
                        postId: postId,
                        userId: userId
                    });

                    UserLikeStore.save().then(async (userlike) => {
                        if (!userlike) {
                            return errorResponse(res, ERROR_INSERT);
                        }
                        Post.findByIdAndUpdate(userlike.postId, { likes: await UserLiked.countDocuments({ postId: userlike.postId }) }).then((post) => {
                            return !post ? errorResponse(res, ERROR_UPDATE) : successResponse(res, null, POST_LIKED)
                        }).catch((e) => errorResponse(res, e))
                    }).catch((e) => errorResponse(res, e))
                } else {
                    if (!found) {
                        return errorResponse(res, POST_LIKE_TWICE)
                    }
                    UserLiked.findByIdAndDelete(found._id).then((userlike) => {
                        return !userlike ? errorResponse(res, ERROR_DELETE) : successResponse(res, null, POST_UNLIKED)
                    }).catch((e) => errorResponse(res, e))
                }
            }).catch((e) => errorResponse(res, e))
        }).catch((e) => errorResponse(res, e))
    }).catch((e) => errorResponse(res, e))
}

export const GetPostById = (req: Request, res: Response) => {
    const id = req.params.id;
    Post.findById(id).populate('userId', 'name username email photo').then((post) => {
        if (!post) {
            return errorResponse(res, DATA_NOT_FOUND)
        }
        let dataPost = {
            id: post._id,
            image: post.image,
            caption: post.caption,
            tags: post.tags,
            likes: post.likes,
            createdAt: post.createdAt,
            updatedAt: post.updatedAt,
            user: post.userId
        }
        return successResponse(res, dataPost);
    }).catch((e) => errorResponse(res, e))
    return true;
}

export const ListPost = (req: Request, res: Response) => {
    const searchBy: string = req.query.searchBy as string;
    const search: string = req.query.search as string;
    const page: number = parseInt(req.query.page as string);
    const limit: number = parseInt(req.query.limit as string);
    const userId: string = req.params.id;
    let query = {
        [searchBy]: { $regex: '.*' + search + '.*' }
    } as any;
    if (userId) {
        query.userId = userId;
    }

    Post.find(query).populate('userId', 'name username email photo').skip(page > 0 ? ((page - 1) * limit) : 0).limit(limit).then((posts) => {
        if (posts.length == 0) {
            return errorResponse(res, DATA_NOT_FOUND)
        }
        let arrayData: Array<object> = [];
        posts.forEach(async (post) => {
            arrayData.push({
                id: post._id,
                image: post.image,
                caption: post.caption,
                tags: post.tags,
                likes: post.likes,
                createdAt: post.createdAt,
                updatedAt: post.updatedAt,
                user: post.userId
            })
        })
        let pagination = {
            total: arrayData.length,
            page: page,
            limit: limit
        }
        return successResponseWithPagination(res, arrayData, pagination);
    }).catch((e) => errorResponse(res, e))
}