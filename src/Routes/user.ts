import express from "express";
import { validate } from "express-validation";
import { UserValidation } from "../Middleware/validation";
import { Register, Login, Logout, UpdateUserData, UpdatePassword, GetUserData } from "../Controller/UserController";
import { verifyToken } from "../Middleware/jwt";
const router = express.Router();

router.post('/register', validate(UserValidation.register), Register);
router.post('/login', validate(UserValidation.login), Login);
router.put('/', validate(UserValidation.updateUser), verifyToken, UpdateUserData);
router.put('/change-password', validate(UserValidation.changePassword), verifyToken, UpdatePassword);
router.get('/', verifyToken, GetUserData);
router.post('/logout', verifyToken, Logout);

export default router;