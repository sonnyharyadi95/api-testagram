import express, { NextFunction, Request, response, Response } from "express";
import userRoutes from "./user";
import postRoutes from "./post";
import imguploadRoutes from "./imgupload";
import { ValidationError } from "express-validation";
import * as responseHelper from "../Helper/Response";
import fileUpload from "express-fileupload";


const app = express();
app.use(fileUpload({
    createParentPath: true,
    limits: {
        fileSize: 1000000,
    },
    abortOnLimit: true
}));

app.get("/", (req: Request, res: Response) => {
    res.status(200).json({
        name: "Default Call to API",
    });
});

app.use("/user", userRoutes);
app.use("/post", postRoutes);
app.use("/upload", imguploadRoutes);

app.use((err: any, req: Request, res: Response, next: NextFunction) => {
    if (err instanceof ValidationError) {
        return responseHelper.validationResponse(res, err.details);
    }
    if (res.statusCode == 401) {
        return responseHelper.unauthorizedResponse(res, err)
    }
});

export default app;