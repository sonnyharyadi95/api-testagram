import express from "express";
import { validate } from "express-validation";
import { PostValidation } from "../Middleware/validation";
import { verifyToken } from "../Middleware/jwt";
import { CreatePost, DeletePost, GetPostById, LikePost, ListPost, UpdatePost } from "../Controller/PostController";
const router = express.Router();

router.post('/', validate(PostValidation.createUpdatePost), verifyToken, CreatePost);
router.put('/:id', validate(PostValidation.createUpdatePost), verifyToken, UpdatePost);
router.delete('/:id', verifyToken, DeletePost);
router.put('/like/:id', verifyToken, LikePost);
router.put('/unlike/:id', verifyToken, LikePost);
router.get('/', validate(PostValidation.listPost), verifyToken, ListPost);
router.get('/:id', verifyToken, GetPostById);
router.get('/user/:id', validate(PostValidation.listPost), verifyToken, ListPost)

export default router;