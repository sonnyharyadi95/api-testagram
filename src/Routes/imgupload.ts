import express from "express";
import { verifyToken } from "../Middleware/jwt";
import { UploadImage } from "../Controller/ImgUploadController";

const router = express.Router();

router.post('/', verifyToken, UploadImage);

export default router;