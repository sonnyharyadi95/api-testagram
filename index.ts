import express, { Request, Response } from "express";
import { connectDb } from "./src/Connection/db";
import dotenv from "dotenv";
import bodyParser from "body-parser";
import { DB_API_PORT } from "./src/Helper/Constant";
import api from "./src/Routes/api";
import path from "path";
import cors from "cors";
dotenv.config();

const app = express();

app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
);
app.use("/file", express.static(path.join(__dirname, 'file')));
app.use(cors());

connectDb();

app.get("/", (req: Request, res: Response) => {
    res.status(200).json({
        name: "Default Route for Testagram",
        version: "1.0.0",
    });
});
app.use("/api", api);

app.listen(DB_API_PORT, () => {
    console.log(`Server Start at PORT ${DB_API_PORT}`)
})