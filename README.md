## Name
API untuk backend testagram

## NOTES!!
untuk keperluan test, .env disertakan ke repo ini

## Description
Terdapat beberapa fungsi yang ada di API ini yaitu
1. User
    - Register: registrasi data user
    - Login: masuk ke dalam sistem dengan autentikasi JWT
    - Update: update informasi user & password
2. Post
    - Create: membuat postingan sesuai user login
    - Update: mengubah data postingan sesuai user login
    - Delete: menghapus postingan berdasarkan id post
    - Like: menyimpan informasi like atas user dan post id, increment nilai likes pada post
    - unlike : menghapus informasi like atas user dan post id, decrement nilai likes pada post
    - Get: mengambil data post berdasarkan id post, id user, dan filter berdasarkan caption / tags
3. Image Upload
    - Upload: upload image dan simpan gambar ke storage, hanya image yang boleh diupload dengan maksimal size 1mb

## Installation
1. clone repo ini ke direktori mana pun
2. buka terminal / bash, jalankan perintah npm install
3. setelah semua node module terpasang, jalankan perintah npm run tsc. perintah ini akan membuat folder build 
4. copy file .env ke dalam folder build

## Usage
untuk menjalankan program, masuk ke direktori build (direktori build akan muncul setelah proses instalasi pada poin 3 sudah dijalankan), kemudian jalankan perintah node index.js